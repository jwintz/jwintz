<p><pre align="center">
<strong>Julien Wintz /</strong> <a href="https://gitlab.inria.fr/jwintz">Gitlab</a> / <a href="https://github.com/jwintz">Github</a> / <a href="https://hal.inria.fr/search/index/?q=julien+wintz&sort=producedDate_tdate+desc">Publications</a></pre></p>

I’m a research engineer at the National Institute for Research in Digital Science and Technology (INRIA, Sophia-Antipolis, FRANCE).<br/>

I got my PhD in algebraic geometry in 2008, and designed _dtk_, a meta platform for modular scientific application development, for which I got, in 2011, Inria prize for supporting research and innovation.
